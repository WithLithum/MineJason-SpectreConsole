# MineJason.Extensions.SpectreConsole

[![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/MineJason%2FMineJason-SpectreConsole?style=flat-square)](https://gitlab.com/MineJason/MineJason-SpectreConsole/-/commits/trunk?ref_type=heads)
[![Discord](https://img.shields.io/discord/1178887806286823424?style=flat-square&logo=discord&logoColor=white&label=%20&color=blue)](https://discord.gg/UFfWb9Rj)
[![QQ](https://img.shields.io/badge/qq%20group-join-blue?style=flat-square
)](https://qm.qq.com/cgi-bin/qm/qr?k=reIRa9w7-vMBemqim7NdREX7vNKirNFo&jump_from=webapi&authKey=UnyZ5LWlfV8g8VCEffm2CShHd9PVPHP5CaXVbxkF2wwZj6FtXGEU/M7jRbU4e/K2)

**简体中文** | [English](README-en.md)

本扩展库提供将 MineJason 的颜色与 Spectre.Console 颜色相互转换以及渲染 MineJason 的文本组件到 Spectre.Console 的功能。

## 用法

建议您安装 `MineJason.Extensions.SpectreConsole` NuGet 包。

### 转换颜色类型

仅支持将 Spectre.Console 的颜色转换为 `RgbChatColor`。

```csharp
using Spectre.Console;
using MineJason.Extensions.SpectreConsole;

var spectreColor = Color.Red;

return spectreColor.ToMineJason();
```

您可以将 `KnownColor` 或 `RgbChatColor` 转换为 Spectre.Console 的 Color；但请注意，
从命名颜色（`KnownColor`）到 Spectre.Console 的转换是不可逆的。

```csharp
using Spectre.Console;
using MineJason.Extensions.SpectreConsole;

Color spectreRed = KnownColor.Red.ToSpectre();
Color spectreRgb = new RgbChatColor(0xFF, 0xFF, 0x00).ToSpectre();
```

### 渲染到控制台

您可以将任何支持的 `ChatComponent` 渲染到控制台。注：不支持 ClickEvent 和 HoverEvent。

```csharp
var component = ChatComponent.CreateText("Hello World!");
var renderer = new AnsiConsoleRenderer();
renderer.Render(component);
```

## 报告问题

您可以前往本项目的[议题区](https://gitlab.com/MineJason/MineJason-SpectreConsole/-/issues)报告问题。

## 许可证

本项目依据 GNU 较宽松通用公共许可证第三版（或您选用的任何更新版本）许可。见其[许可证文本](COPYING.LESSER.txt)；另见 [GPL 许可证文本](COPYING.txt)。

本项目许可证的 SPDX 许可证编号为 `LGPL-3.0-or-later`。
