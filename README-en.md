# MineJason.Extensions.SpectreConsole

[![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/MineJason%2FMineJason-SpectreConsole?style=flat-square)](https://gitlab.com/MineJason/MineJason-SpectreConsole/-/commits/trunk?ref_type=heads)
[![Discord](https://img.shields.io/discord/1178887806286823424?style=flat-square&logo=discord&logoColor=white&label=%20&color=blue)](https://discord.gg/UFfWb9Rj)
[![QQ](https://img.shields.io/badge/qq%20group-join-blue?style=flat-square
)](https://qm.qq.com/cgi-bin/qm/qr?k=reIRa9w7-vMBemqim7NdREX7vNKirNFo&jump_from=webapi&authKey=UnyZ5LWlfV8g8VCEffm2CShHd9PVPHP5CaXVbxkF2wwZj6FtXGEU/M7jRbU4e/K2)

[简体中文](README.md) | **English**

An extension library to MineJason that provides conversion of colour types between MineJason ones and Spectre.Console ones, and rendering of MineJason components to Spectre.Console.

## Usage

It is recommended that you install the `MineJason.Extensions.SpectreConsole` package.

### Converting colours

Only converting to `RgbChatColor` from Spectre.Console colours is supported.

```csharp
using Spectre.Console;
using MineJason.Extensions.SpectreConsole;

var spectreColor = Color.Red;

return spectreColor.ToMineJason();
```

You can convert `KnownColor` or `RgbChatColor` to Spectre.Console colour types; but beware that `KnownColor` conversion is irreversible.

```csharp
using Spectre.Console;
using MineJason.Extensions.SpectreConsole;

Color spectreRed = KnownColor.Red.ToSpectre();
Color spectreRgb = new RgbChatColor(0xFF, 0xFF, 0x00).ToSpectre();
```

### Render to console

Any supported `ChatComponent` can be rendered to the console. Note: ClickEvent and HoverEvent are ignored.

```csharp
var component = ChatComponent.CreateText("Hello World!");
var renderer = new AnsiConsoleRenderer();
renderer.Render(component);
```

## Reporting Issues

Go to the [Issues](https://gitlab.com/MineJason/MineJason-SpectreConsole/-/issues) section to report issues.

## Licensing

This library is available under GNU Lesser General Public License, either version 3 or (at your opinion) any later version.

See [its license text](COPYING.LESSER.txt) and the [GPL license text](COPYING.txt) for details.

The SPDX license ID is `LGPL-3.0-or-later`.
