﻿namespace MineJason.Extensions.SpectreConsole;
using System;

/// <summary>
/// Configuration entries for <see cref="AnsiConsoleRenderer"/>.
/// </summary>
public sealed class AnsiConsoleRendererSettings : IEquatable<AnsiConsoleRendererSettings>
{
    /// <summary>
    /// Gets or sets a value indicating whether the fallback value is used when
    /// a translatable component with a fallback is rendered.
    /// </summary>
    public bool UseFallbackForTranslatable { get; set; } = false;

    /// <inheritdoc />
    public bool Equals(AnsiConsoleRendererSettings? other)
    {
        return other != null &&
            other.UseFallbackForTranslatable == UseFallbackForTranslatable;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return Equals(obj as AnsiConsoleRendererSettings);
    }

    /// <summary>
    /// Gets the hash code of this instance.
    /// </summary>
    /// <returns>The hash code.</returns>
    public override int GetHashCode()
    {
        var hashCode = new HashCode();
        hashCode.Add(UseFallbackForTranslatable);
        return hashCode.ToHashCode();
    }
}
