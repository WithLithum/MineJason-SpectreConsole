﻿namespace MineJason.Extensions.SpectreConsole;

using MineJason.Components;
using Spectre.Console;

/// <summary>
/// Renders chat components to the console via Spectre Console.
/// </summary>
public class AnsiConsoleRenderer
{
    /// <summary>
    /// Initialises a new instance of the <see cref="AnsiConsoleRenderer"/> class.
    /// </summary>
    public AnsiConsoleRenderer() : this(new())
    {
    }

    /// <summary>
    /// Initialises a new instance of the <see cref="AnsiConsoleRenderer"/> class.
    /// </summary>
    /// <param name="settings">The settings to use.</param>
    public AnsiConsoleRenderer(AnsiConsoleRendererSettings settings)
    {
        Settings = settings;
    }

    /// <summary>
    /// Gets the settings of this instance.
    /// </summary>
    public AnsiConsoleRendererSettings Settings { get; }

    internal string GetContent(ChatComponent component)
    {
        if (component is TextChatComponent text)
        {
            return text.Text;
        }

        if (component is TranslatableChatComponent translatable)
        {
            // If configured, use the fallback.
            if (Settings.UseFallbackForTranslatable
                && translatable.Fallback != null)
            {
                return translatable.Fallback;
            }

            // Returns the translatable string ID as the game normally would when such translatable
            // text is not found.
            return translatable.Translate;
        }

        if (component is EntityChatComponent entity)
        {
            return $"(entity: {entity.Selector})";
        }

        if (component is ScoreboardChatComponent scoreboard)
        {
            return $"(scores: {scoreboard.Score})";
        }

        if (component is BlockNbtChatComponent block)
        {
            return $"(block nbt: {block.Block}, {block.Path})";
        }

        if (component is EntityNbtChatComponent entityNbt)
        {
            return $"(entity nbt: {entityNbt.Entity}, {entityNbt.Path})";
        }

        if (component is StorageNbtChatComponent storage)
        {
            return $"(storage nbt: {storage.Storage}, {storage.Path})";
        }

        if (component is KeybindChatComponent keybind)
        {
            return keybind.Keybind;
        }

        return "(unknown component)";
    }

    internal static Color GetColor(IChatColor? color)
    {
        return (color ?? KnownColor.Gray).ToSpectre();
    }

    internal static Decoration GetDecoration(ChatComponent component, ChatComponent? parent)
    {
        static bool GetParentValue(bool? subject, bool? parent)
        {
            return subject ?? parent ?? false;
        }

        var bold = GetParentValue(component.Bold, parent?.Bold);
        var italic = GetParentValue(component.Italic, parent?.Italic);
        var underlined = GetParentValue(component.Underline, parent?.Underline);
        var strikethrough = GetParentValue(component.Strikethrough, parent?.Strikethrough);

        var decoration = Decoration.None;

        if (bold)
        {
            decoration |= Decoration.Bold;
        }

        if (italic)
        {
            decoration |= Decoration.Italic;
        }

        if (underlined)
        {
            decoration |= Decoration.Underline;
        }

        if (strikethrough)
        {
            decoration |= Decoration.Strikethrough;
        }

        return decoration;
    }

    /// <summary>
    /// Renders the specified chat component, with style devired from its parent, to the console.
    /// </summary>
    /// <param name="component">The component to render.</param>
    /// <param name="parent">The parent component to devire style from.</param>
    public void Render(ChatComponent component, ChatComponent? parent = null)
    {
        var style = new Style(GetColor(component.Color), Color.Black, GetDecoration(component, parent));
        var content = GetContent(component);

        var markup = new Markup(content, style);
        AnsiConsole.Write(markup);

        if (component.Extra != null)
        {
            foreach (var child in component.Extra)
            {
                Render(child, component);
            }
        }
    }
}
