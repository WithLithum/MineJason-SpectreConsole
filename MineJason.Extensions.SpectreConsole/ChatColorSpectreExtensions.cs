﻿namespace MineJason.Extensions.SpectreConsole;

using MineJason.Colors;
using Spectre.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Provides extension methods for working with <see cref="IChatColor"/> instances alongside with
/// the Spectre Console library.
/// </summary>
public static class ChatColorSpectreExtensions
{
    private static readonly Dictionary<KnownColor, Color> ColorPairs = new()
    {
        { KnownColor.Black, Color.Black },
        { KnownColor.DarkBlue, Color.DarkBlue },
        { KnownColor.DarkGreen, Color.DarkGreen },
        { KnownColor.DarkAqua, new Color(0x00, 0xAA, 0xAA) },
        { KnownColor.DarkRed, Color.DarkRed },
        { KnownColor.DarkPurple, new Color(0xAA, 0x00, 0xAA) },
        { KnownColor.Gold, new Color(0xFF, 0xAA, 0x00) },
        { KnownColor.Gray, new Color(0xAA, 0xAA, 0xAA) },
        { KnownColor.DarkGray, new Color(0x55, 0x55, 0x55) },
        { KnownColor.Blue, new Color(0x55, 0x55, 0xFF) },
        { KnownColor.Green, new Color(0x55, 0xFF, 0x55) },
        { KnownColor.Aqua, new Color(0x55, 0xFF, 0xFF) },
        { KnownColor.Red, new Color(0xFF, 0x55, 0x55) },
        { KnownColor.LightPurple, new Color(0xFF, 0x55, 0xFF) },
        { KnownColor.Yellow, new Color(0xFF, 0xFF, 0x55) },
        { KnownColor.White, Color.White }
    };

    /// <summary>
    /// Converts this instance to its Spectre Console equivalent.
    /// </summary>
    /// <param name="color">The color to convert from.</param>
    /// <returns>The converted result.</returns>
    /// <exception cref="ArgumentException">Invalid known color name.</exception>
    public static Color ToSpectre(this KnownColor color)
    {
        if (!ColorPairs.TryGetValue(color, out var value))
        {
            throw new ArgumentException(MineJasonSpectreMessages.InvalidKnownChatColor,
                nameof(color));
        }

        return value;
    }

    /// <summary>
    /// Converts this instance to its Spectre Console equivalent.
    /// </summary>
    /// <param name="color">The color to convert from.</param>
    /// <returns>The converted result.</returns>
    public static Color ToSpectre(this RgbChatColor color)
    {
        return new(color.R, color.G, color.B);
    }

    /// <summary>
    /// Converts this instance to its Spectre Console equivalent.
    /// </summary>
    /// <param name="color">The color to convert from.</param>
    /// <returns>The converted result.</returns>
    /// <exception cref="ArgumentException">Custom implementations of <see cref="IChatColor"/> aren't supported.</exception>
    public static Color ToSpectre(this IChatColor color)
    {
        if (color is KnownColor known)
        {
            return known.ToSpectre();
        }

        if (color is RgbChatColor rgb)
        {
            return rgb.ToSpectre();
        }

        throw new ArgumentException(MineJasonSpectreMessages.ChatColorCustomImpl,
            nameof(color));
    }

    /// <summary>
    /// Converts this instance to its MineJason equivalent.
    /// </summary>
    /// <param name="color">The color to convert from.</param>
    /// <returns>The converted result.</returns>
    public static RgbChatColor ToMineJason(this Color color)
    {
        return new RgbChatColor(color.R, color.G, color.B);
    }
}
